import mailChannelsPlugin from "@cloudflare/pages-plugin-mailchannels";

export const onRequest: PagesFunction = mailChannelsPlugin({
  personalizations: [
    {
      to: [{ name: "Darshani Audhish", email: "iam@darshaniaudhish.com" }],
    },
  ],
  from: {
    name: "Darshani Audhish",
    email: "iam@darshaniaudhish.com",
  },
  respondWith: () => {
    return new Response(
      `Thank you for submitting your enquiry. A member of the team will be in touch shortly.`
    );
  },
});